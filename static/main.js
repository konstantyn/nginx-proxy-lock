angular.module('demoApp', [])
    .controller('DemoController', ['$scope', '$http', function ($scope, $http) {
        $scope.responses = [];
        $scope.api_url = '/pow/5_5';
        $scope.test = function () {
            for (var i = 0; i < 10; i++) {
                aaa($scope, $http, i);
            }
        }

    }]);

function aaa($scope, $http, i) {
    $http.get($scope.api_url)
        .then(function (response) {
            $scope.responses.push({
                num: $scope.responses.length + 1,
                body: response.data,
                i: i
            });
        });
}