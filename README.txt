This is a demonstration how to configure nginx as caching proxy 
which will make only one request to upstream server. Other requests 
will be locked before response from upstream server will be  received.

1. Open conf/default.conf file and change proxy_pass to your local IP address.

2. Run docker nginx:
    ./run-nginx-docker.sh

3. Run Flask app:
    python app.py

4. Open link in browser, open broeser console and click test:
    http://localhost:8081/static/index.html

Now you can see that browser made 10 requests when application server received only one request. 

5. Stop Flask by CTRL+C and stop docker nginx:
    ./stop-nginx-docker.sh
