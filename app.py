from time import sleep
from flask import Flask
from flask_cors import CORS

app = Flask(__name__)
CORS(app)


@app.route("/")
def hello():
    return '<a href="/static/index.html">Demo</a>'


@app.route("/pow/<x>_<y>")
def pow(x, y):
    sleep(1)
    return '{}'.format(float(x) ** float(y))


if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')
